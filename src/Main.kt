import loader.Loader

/**
 * Created by Michał on 2019-01-12.
 */
object Main {

    @JvmStatic
    fun main(args: Array<String>) {

//        Loader().loadAssociations().process()
//        Loader().loadClassification().process()
        Loader().loadSegmentation().process()
    }
}
