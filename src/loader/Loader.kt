package loader

import model.Association
import model.Classification
import model.Segmentation
import java.io.File

/**
 * Created by Michał on 2019-01-12.
 */
class Loader {
    private val associationFileName = "asocjacja.txt"
    private val classificationFileName = "klasyfikacja.txt"
    private val toPredictFileName = "toDo.txt"
    private val segmentationFileName = "segmentacja.txt"


    fun loadAssociations(): Association {
        val values = arrayListOf<List<String>>()
        File(associationFileName).forEachLine {
            val line = it.substring(1, it.length - 1).split(",").map(String::trim)
            values.add(line)
        }

        return Association(values)
    }


    fun loadClassification(): Classification {
        val values = arrayListOf<List<String>>()
        File(classificationFileName).forEachLine {
            val line = it.split(" ").map(String::trim)
            values.add(line)
        }

        val valuesToPredict = arrayListOf<List<String>>()
        File(toPredictFileName).forEachLine {
            val line = it.split(" ").map(String::trim)
            valuesToPredict.add(line)
        }

        return Classification(values, valuesToPredict)
    }

    fun loadSegmentation(): Segmentation {
        val values = arrayListOf<List<Double>>()
        File(segmentationFileName).forEachLine {
            val line = it.substring(1, it.length - 1).split(", ").map(String::trim).map(String::toDouble)
            values.add(line)
        }

        return Segmentation(values)
    }
}