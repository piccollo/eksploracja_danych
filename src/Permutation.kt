/**
 * Created by Michał on 2019-01-12.
 */

object Permutation {

    fun permuteList(products: List<String>, numberOfElements: Int): List<List<String>> {
        val result = arrayListOf<ArrayList<String>>()
        permute(products, numberOfElements, result = result)
        result.removeIf { list -> list.groupingBy { it }.eachCount().map { it.value }.any { it > 1 } }
        return result.map { it.sorted() }.distinct()
    }

    private fun permute(products: List<String>, numberOfElements: Int, singleResult: ArrayList<String> = arrayListOf(), result: ArrayList<ArrayList<String>>) {
        if (singleResult.size == numberOfElements) {
            result.add(singleResult)
            return
        }

        products.forEach {
            val temp = arrayListOf(*singleResult.toTypedArray())
            temp.add(it)
            permute(products, numberOfElements, temp, result)
        }
    }
}
