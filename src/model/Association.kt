package model

import Permutation

/**
 * Created by Michał on 2019-01-12.
 */
data class Association(val products: List<List<String>>) {
    private val minSup = 10
    val distinctProducts: ArrayList<String> = findDistinctProducts()
    private val supportedProducts = distinctProducts.map { listOf(it) }.toMutableList()

    private fun findDistinctProducts(): ArrayList<String> {
        return products.flatMap { it }.distinct() as ArrayList<String>
    }

    fun process() {
        val productMetConditions = listOf<List<String>>().toMutableList()

        supportedProducts.forEach { distinctProducts ->
            val count = products.count { it.containsAll(distinctProducts) }.toDouble()
            if (count > minSup) {
                productMetConditions.add(distinctProducts)
            }
        }

        var lastKnownProducts = ""

        var round = 1
        while (productMetConditions.size > 0 || round > 10) {
            lastKnownProducts = productMetConditions.flatMap { it }.distinct().toString()
            productMetConditions.clear()
            round++
            println("The round -> $round")

            supportedProducts.forEach { distinctProducts ->
                val count = products.count { it.containsAll(distinctProducts) }.toDouble()
                if (count > minSup) {
                    productMetConditions.add(distinctProducts)
                }
            }
            println(productMetConditions.take(2))
            supportedProducts.clear()
            supportedProducts.addAll(Permutation.permuteList(productMetConditions.flatMap { it }.distinct(), round).toMutableList())
        }

        println(lastKnownProducts)
    }
}