package model

import java.util.*

/**
 * Created by Michał on 2019-01-16.
 */
data class Segmentation(val values: List<List<Double>>) {

    fun process() {
        val max = values.flatMap { it }.max()
        val min = values.flatMap { it }.min()

//        println("Max: $max and Min: $min")

        val midPoints = getRandomPoints(3, min!!, max!!)
        val groups = hashMapOf<List<Double>, ArrayList<ArrayList<Double>>>()
        midPoints.forEach { groups.put(it, arrayListOf()) }

        for (n in 0..10) {
            groups.clear()
            midPoints.forEach { groups.put(it, arrayListOf()) }

            values.forEach { point ->
                var distance = 1000.0
                var closestMidPoint = listOf<Double>()

                groups.keys.forEach { midPoint ->
                    val pointValue = Math.pow(midPoint[0] - point[0], 2.0) + Math.pow(midPoint[1] - point[1], 2.0) +
                            Math.pow(midPoint[2] - point[2], 2.0) + Math.pow(midPoint[3] - point[3], 2.0) + Math.pow(midPoint[4] - point[4], 2.0)
                    val tempDistance = Math.sqrt(Math.abs(pointValue))

                    if (distance > tempDistance) {
                        distance = tempDistance
                        closestMidPoint = midPoint
                    }
                }
                groups[closestMidPoint]?.add(point as ArrayList<Double>)
            }

            val newMidPoints = groups.map { entry ->
                val reducedPoints = entry.value.reduce { acc, arrayList ->
                    arrayListOf(acc[0] + arrayList[0], acc[1] + arrayList[1],
                            acc[2] + arrayList[2], acc[3] + arrayList[3], acc[4] + arrayList[4])
                }
                val numberOfPointsInAGroup = entry.value.size
                arrayListOf(reducedPoints[0] / numberOfPointsInAGroup, reducedPoints[1] / numberOfPointsInAGroup,
                        reducedPoints[2] / numberOfPointsInAGroup, reducedPoints[3] / numberOfPointsInAGroup, reducedPoints[4] / numberOfPointsInAGroup)
            }

            midPoints.clear()
            midPoints.addAll(newMidPoints)
        }

        /*groups.forEach { key, values ->
            println("Group: $key ->> ${values.size}")
        }*/


        for (k in 1..5) {
            println("K= $k")
            groups.forEach { key, values ->
                println("Point: $key ->> closes $k points: ${values.take(k)}")
            }
            println()
        }


    }

    private fun getRandomPoints(numberOfPoints: Int, min: Double, max: Double): ArrayList<List<Double>> {
        val randomPoints = arrayListOf<List<Double>>()

        for (n in 0..numberOfPoints) {
            val point = arrayListOf(
                    Random().nextDouble() * max + min,
                    Random().nextDouble() * max + min,
                    Random().nextDouble() * max + min,
                    Random().nextDouble() * max + min,
                    Random().nextDouble() * max + min
            )
            randomPoints.add(point)
        }

        return randomPoints
    }
}