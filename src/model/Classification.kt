package model

/**
 * Created by Michał on 2019-01-14.
 */
class Classification(val values: List<List<String>>, val valuesToPredict: List<List<String>>) {
    private val minimalPercentValue = 50

    fun process() {
        valuesToPredict.forEach { suspect ->
            if (values.filter { it[2] == suspect[2] && it[3] == suspect[3] && it[4] == suspect[4] && it[5] == suspect[5] && it[6] == suspect[6] }.all { it[7] == "false" }) {
                println("$suspect nie dostanie kredytu")
                return@forEach
            } else if (values.filter { it[2] == suspect[2] && it[3] == suspect[3] && it[4] == suspect[4] && it[5] == suspect[5] && it[6] == suspect[6] }.all { it[7] == "true" }) {
                println("$suspect  dostanie kredyt")
                return@forEach
            }

            val falseCount = values.filter { it[2] == suspect[2] && it[3] == suspect[3] && it[4] == suspect[4] && it[5] == suspect[5] && it[6] == suspect[6] }.count { it[7] == "false" }
            val trueCount = values.filter { it[2] == suspect[2] && it[3] == suspect[3] && it[4] == suspect[4] && it[5] == suspect[5] && it[6] == suspect[6] }.count { it[7] == "true" }
            if ((trueCount * 1.0 / (trueCount + falseCount)) * 100 > minimalPercentValue) {
                println("$suspect  dostanie kredyt")
            }
        }
    }
}